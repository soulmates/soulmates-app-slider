from django.contrib import admin
from soulmates_app_slider.models import *
# Register your models here.
from adminsortable.admin import NonSortableParentAdmin, SortableTabularInline
from django.contrib import admin
from soulmates_app_common.admin import get_admin_thumb

class SlideInline(SortableTabularInline):
    model = Slide
    fields = ('image_thumb', 'image',)
    extra = 1
    show_change_link = True
    readonly_fields = ['image_thumb']
    

class SliderAdmin(NonSortableParentAdmin):
    inlines = [
        SlideInline,
    ]
    
    

admin.site.register(Slider, SliderAdmin)