from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.template.loader import render_to_string
from soulmates_app_slider.models import Slider

        

# @start_from {int} - slice query from
# @end_to {int} - slice query to
class SliderContentType(models.Model):
    slider = models.ForeignKey(Slider, on_delete=models.CASCADE, verbose_name=_('slider'))
    
    def render(self, **kwargs):
        slides = self.slider.slide_set.all()
        return render_to_string('slider/slider.html', {
            'slides': slides,
            })
            
    class Meta:
        abstract = True
        verbose_name = _('slider block')
        verbose_name_plural = _('slider blocks')