from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from cloudinary.models import CloudinaryField
from adminsortable.fields import SortableForeignKey
from adminsortable.models import SortableMixin
from soulmates_app_common.admin import get_admin_thumb
from django.utils.encoding import python_2_unicode_compatible


# Create your models here.

@python_2_unicode_compatible
class Slider(models.Model):
    
    # fields
    title = models.CharField(max_length=255, verbose_name=_('title'))
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('created at'))
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('update at'))
        
    # methods
    def __str__(self):
        return self.title
        
    class Meta:
        verbose_name = _('Slider')
        verbose_name_plural = _('Sliders')
        ordering = ['-created_at']
        
        
class Slide(SortableMixin):
    slider = SortableForeignKey(Slider, on_delete=models.CASCADE)
    sort_order = models.IntegerField(default=0, editable=False, db_index=True)
    image = CloudinaryField(_('image'), blank=True, null=True)
    image_thumb = get_admin_thumb
    
    def __str__(self):
        return '%s %s'%(_('slide'), self.sort_order)
        
    class Meta:
        verbose_name = _('Slide')
        verbose_name_plural = _('Slides')
        ordering = ['sort_order']